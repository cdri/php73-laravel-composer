# php7.3 + extensions

Fork from `matt9mg/php7.3` image +

* composer
* php-pear
  * php7.3-bcmath
  * php7.3-ctype
  * php7.3-json
  * php7.3-mbstring
  * php7.3-pdo
  * php7.3-tokenizer
  * php7.3-xml
  * php7.3-zip
  * php7.3-mysql
  * php7.3-mongodb
  * php7.3-memcached
  * php7.3-xdebug
